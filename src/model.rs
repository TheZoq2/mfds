use crate::layout::{self, Layer};
use crate::calc::Calculator;
use arrayvec::ArrayVec;

pub struct Model {
    pub layer_stack: ArrayVec<[&'static Layer; 30]>,
    pub current_layer: &'static Layer,
    pub calculator: Calculator,
    pub calculator_active: bool,
}

impl Model {
    pub fn new(current_layer: &'static Layer) -> Self {
        Self {
            layer_stack: ArrayVec::new(),
            current_layer,
            calculator: Calculator::new(),
            calculator_active: false,
        }
    }

    pub fn push_layer(&mut self, layer: &'static Layer) {
        self.layer_stack.push(self.current_layer);
        self.current_layer = layer;
    }
    pub fn pop_layer(&mut self) {
        self.current_layer = self.layer_stack.pop().unwrap();
    }

    pub fn show_calculator(&mut self) {
        self.push_layer(layout::calculator::MAIN_LAYER);
        self.calculator_active = true;
    }
    pub fn hide_calculator(&mut self) {
        self.pop_layer();
        self.calculator_active = false;
    }
}
