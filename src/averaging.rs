const AVERAGE_WINDOW: usize = 10;

pub struct Averaging {
    samples: [u16; AVERAGE_WINDOW],
    offset: usize
}

impl Averaging {
    pub fn new() -> Self {
        Self {samples: [0; AVERAGE_WINDOW], offset: 0}
    }

    pub fn add_sample(&mut self, sample: u16) {
        self.samples[self.offset] = sample;
        self.offset = (self.offset + 1) % self.samples.len()
    }

    pub fn current(&self) -> u16 {
        self.samples.iter().sum::<u16>() / self.samples.len() as u16
    }
}


