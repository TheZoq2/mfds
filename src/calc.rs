use arrayvec::{ArrayString, ArrayVec};

#[derive(Clone, PartialEq)]
pub enum Operand {
    Stored(usize),
    Literal(f32),
}

#[derive(Clone, PartialEq)]
pub enum Operation {
    Nop,
    Add,
    Sub,
    Qfe
}

const MAX_OPERANDS: usize = 3;

#[derive(Clone, PartialEq)]
pub struct Calculator {
    pub stored_values: [Option<f32>; 10],
    pub next_storage: usize,

    pub operands: ArrayString<[u8; 32]>,
}


impl Calculator {
    pub fn new() -> Self {
        Self {
            stored_values: [None; 10],
            next_storage: 0,
            operands: ArrayString::new(),
        }
    }

    pub fn push_char(&mut self, c: char) {
        self.operands.push(c)
    }

    pub fn erase(&mut self) {
        if !self.operands.is_empty() {
            self.operands.pop();
        }
    }
}
