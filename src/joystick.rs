// Borrowed and modified from
// https://github.com/TeXitoi/keyberon/blob/master/src/keyboard.rs

use crate::hid::{HidDevice, Protocol, ReportType, Subclass};
// use crate::key_code::KbHidReport;

const BUTTON_COUNT: usize = 64;


// Stolen from the working example at https://stackoverflow.com/questions/14904009/simple-joystick-hid-report-descriptor-doesnt-work
const REPORT_DESCRIPTOR: &[u8] = &[
    0x0E, 0x0C, 0xCA,  // Unknown (bTag: 0x00, bType: 0x03)
    0x48,              // Designator Minimum
    0x05, 0x01,        // Usage Page (Generic Desktop Ctrls)
    0x09, 0x04,        // Usage (Joystick)
    0xA1, 0x01,        // Collection (Application)
    0x05, 0x09,        //   Usage Page (Button)
    0x19, 0x01,        //   Usage Minimum (0x01)
    0x29, BUTTON_COUNT as u8,        //   Usage Maximum
    0x15, 0x00,        //   Logical Minimum (0)
    0x25, 0x01,        //   Logical Maximum (1)
    0x75, 0x01,        //   Report Size (1)
    0x95, BUTTON_COUNT as u8,        //   Report Count
    0x55, 0x00,        //   Unit Exponent (0)
    0x65, 0x00,        //   Unit (None)
    0x81, 0x02,        //   Input (Data,Var,Abs,No Wrap,Linear,Preferred State,No Null Position)
    0xC0,              // End Collection
];


#[derive(Clone)]
pub struct ButtonReport {
    pub buttons: [bool; BUTTON_COUNT],
}

impl ButtonReport {
    pub fn new(buttons: [bool; BUTTON_COUNT]) -> Self {
        Self {buttons}
    }
    pub fn as_bytes(&self) -> [u8; 8] {
        let mut result = [0;8];

        for (i, button) in self.buttons.iter().enumerate() {
            let byte = i / 8;
            let offset = i % 8;
            result[byte] |= (*button as u8) << offset;
        }
        result
    }
}

impl Default for ButtonReport {
    fn default() -> Self {
        Self {
            buttons: [false; BUTTON_COUNT],
        }
    }
}



pub struct Joystick {
    report: [u8; 8],
}
impl Joystick {
    pub fn new() -> Self {
        Self {
            report: ButtonReport::default().as_bytes(),
        }
    }
    pub fn set_report(&mut self, report: ButtonReport) {
        self.report = report.as_bytes();
    }
}

impl HidDevice for Joystick {
    fn subclass(&self) -> Subclass {
        Subclass::None
    }

    fn protocol(&self) -> Protocol {
        Protocol::None
    }

    fn report_descriptor(&self) -> &[u8] {
        REPORT_DESCRIPTOR
    }

    fn get_report(&mut self, report_type: ReportType, _report_id: u8) -> Result<&[u8], ()> {
        match report_type {
            ReportType::Input => Ok(&self.report),
            _ => Err(()),
        }
    }

    fn set_report(
        &mut self,
        _report_type: ReportType,
        _report_id: u8,
        _data: &[u8],
    ) -> Result<(), ()> {
        Ok(())
    }
}
