#[macro_export]
macro_rules! define_scanner {
    ($name:ident, $inputs:expr, $outputs:expr) => {
        pub struct $name<I: InputPin, O: OutputPin> {
            inputs: [I; $inputs],
            outputs: [O; $outputs],
            // Time since last change on pin. Used for debouncing
            last_value: [[bool; $inputs]; $outputs],
            last_activity: [[u16; $inputs]; $outputs],
            held_previous_layer: [[bool; $inputs]; $outputs],
            // Value of the timer last time we saw it. Used to check for overflows
            last_timer: u32
        }

        impl<I, O> $name<I, O>
            where I: InputPin,
                  O: OutputPin,
                  I::Error: core::fmt::Debug,
                  O::Error: core::fmt::Debug
        {
            pub fn new(inputs: [I; $inputs], outputs: [O; $outputs]) -> Self {
                Self {
                    inputs, outputs,
                    last_activity: [[0; $inputs]; $outputs],
                    last_value: [[false; $inputs]; $outputs],
                    held_previous_layer: [[false; $inputs]; $outputs],
                    last_timer: 0,
                }
            }

            /**
              Scans through the key matrix, updates the currently pressed
              keys and returns the keys that changed from released to
              pressed this iteration
            */
            pub fn scan(
                &mut self,
                time: u32,
            ) -> ArrayVec<[(u8, u8); $inputs*$outputs]>{
                // Check the whole key matrix for presses and store them
                // for debouncing
                let mut candidates = [[false; $inputs]; $outputs];
                for (x, output) in self.outputs.iter_mut().enumerate() {
                    output.set_low().unwrap();
                    asm::delay(100);
                    for (y, input) in self.inputs.iter().enumerate() {
                        if input.is_low().unwrap() {
                            candidates[x][y] = true;
                        }
                    }
                    output.set_high().unwrap();
                }
                let mut result = ArrayVec::new();
                // This requires 2 for loops because we need mutable borrows
                // to self above
                for o in 0..$outputs {
                    for i in 0..$inputs {
                        let is_bouncing = self.do_debounce((o, i), time);
                        if !is_bouncing {
                            // If the button changed since last time we checked
                            if self.last_value[o][i] != candidates[o][i] {
                                let val =
                                    candidates[o][i] && !self.held_previous_layer[o][i];
                                self.last_value[o][i] = val;
                                self.last_activity[o][i] = time as u16;
                                if val {
                                    result.push((o as u8, i as u8))
                                }
                            }
                            // If the button is not bouncing, and is now released,
                            // it can be pressed again
                            if !candidates[o][i] {
                                self.held_previous_layer[o][i] = false;
                            }
                        }
                        if self.last_value[o][i] != candidates[o][i] && !is_bouncing {
                        }
                    }
                }
                self.last_timer = time;
                result
            }

            fn pressed(&self) -> &[[bool; $inputs]; $outputs] {
                &self.last_value
            }

            pub fn notify_layer_change(&mut self) {
                // Remember that all buttons may have been pressed before the layer
                // change
                self.held_previous_layer = [[true;$inputs];$outputs];
                // Reset the pressed state of each button
                self.last_value = [[false;$inputs];$outputs];
            }

            fn do_debounce(&self, (o,i): (usize, usize), time: u32) -> bool{
                let bounce_threshold = 1000;
                // Handle overflow in the timer
                let mut new_time: u32 = time & 0x0000ffff;
                // If 16 bit overflow occured, the new time should be
                // offset
                if (time & 0xffff0000) != (self.last_timer & 0xffff0000) {
                    new_time += 0x0000ffff;
                }

                let prev_activity = self.last_activity[o][i];

                new_time > (prev_activity as u32) + bounce_threshold
            }
        }
    }
}
