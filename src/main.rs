#![no_std]
#![no_main]

extern crate cortex_m;
extern crate cortex_m_rt as rt;
extern crate panic_semihosting;
extern crate stm32f1xx_hal;

use rtfm::app;

use embedded_hal::digital::v2::{InputPin, OutputPin};
use stm32f1xx_hal::{
    prelude::*,
    pac,
    delay::Delay,
    usb,
    gpio::{
        Output,
        PushPull,
        PullUp,
        Input,
        Pxx,
    },
    timer::{Timer, CountDownTimer},
    device::SYST,
};
use usb_device::prelude::*;
use usb_device::bus::UsbBusAllocator;
use usb_device::class::UsbClass;
use usbd_serial::{SerialPort, USB_CLASS_CDC};
use cortex_m::asm;
use ufmt::uwriteln;
use arrayvec::ArrayVec;

mod hid;
mod button_scanner;
mod layout;
mod display;
mod joystick;
mod calc;
mod model;

use layout::{CalcAction, Action};
use display::Display;
use model::Model;

const GRID_SIZE_X: usize = 7;
const GRID_SIZE_Y: usize = 5;
define_scanner!(Scanner, GRID_SIZE_X, GRID_SIZE_Y);



#[app(device = stm32f1xx_hal::pac, peripherals = true)]
const APP: () = {
    struct Resources {
        usb_bus: &'static UsbBusAllocator<usb::UsbBusType>,
        usb_serial: usbd_serial::SerialPort<'static, usb::UsbBusType>,
        usb_joystick: hid::HidClass<'static, usb::UsbBusType, joystick::Joystick>,
        usb_device: UsbDevice<'static, usb::UsbBusType>,
        timer: CountDownTimer<pac::TIM2>,
        scanner: Scanner<Pxx<Input<PullUp>>, Pxx<Output<PushPull>>>,
        display: Display,
        model: Model,
        syst: SYST,
    }

    #[init]
    fn init(ctx: init::Context) -> init::LateResources {
        static mut USB_BUS: Option<UsbBusAllocator<usb::UsbBusType>> = None;

        // Alias peripherals
        let cp: cortex_m::Peripherals = ctx.core;
        let dp: pac::Peripherals = ctx.device;

        // Set up core registers
        let mut flash = dp.FLASH.constrain();
        let mut rcc = dp.RCC.constrain();

        let clocks = rcc.cfgr
            .use_hse(16.mhz())
            .sysclk(48.mhz())
            .pclk1(24.mhz())
            .pclk2(48.mhz())
            .freeze(&mut flash.acr);

        assert!(clocks.usbclk_valid());

        let mut afio = dp.AFIO.constrain(&mut rcc.apb2);

        // Set up GPIO registers
        let mut gpioa = dp.GPIOA.split(&mut rcc.apb2);
        let mut gpiob = dp.GPIOB.split(&mut rcc.apb2);
        let mut gpioc = dp.GPIOC.split(&mut rcc.apb2);


        // Set up the USB port
        let mut usb_dp = gpioa.pa12.into_push_pull_output(&mut gpioa.crh);
        usb_dp.set_low().unwrap();
        asm::delay(clocks.sysclk().0 / 100);

        let usb = usb::Peripheral {
            usb: dp.USB,
            pin_dm: gpioa.pa11,
            pin_dp: usb_dp.into_floating_input(&mut gpioa.crh),
        };

        let (usb_serial, usb_joystick, usb_device) = {
            *USB_BUS = Some(usb::UsbBus::new(usb));
            let serial = SerialPort::new(USB_BUS.as_ref().unwrap());

            let joystick = hid::HidClass::new(joystick::Joystick::new(), USB_BUS.as_ref().unwrap());

            let usb_dev = UsbDeviceBuilder::new(
                        USB_BUS.as_ref().unwrap(),
                        UsbVidPid(0x16c0, 0x27df)
                    )
                .manufacturer("ZoqsHOTAS")
                .product("MFD")
                .serial_number("TEST")
                .device_class(USB_CLASS_CDC)
                .build();

            // (serial, (), usb_dev)
            (serial, joystick, usb_dev)
        };

        // Configure delay
        let mut delay = Delay::new(cp.SYST, clocks);

        // Set up the display
        let display = display::Display::new(
            dp.SPI1,
            display::DisplayPins {
                sck: gpioa.pa5.into_alternate_push_pull(&mut gpioa.crl),
                miso: gpioa.pa6.into_floating_input(&mut gpioa.crl),
                mosi: gpioa.pa7.into_alternate_push_pull(&mut gpioa.crl),

                rst: gpiob.pb0.into_push_pull_output(&mut gpiob.crl),
                dc: gpiob.pb1.into_push_pull_output(&mut gpiob.crl),
                backlight: gpiob.pb10.into_push_pull_output(&mut gpiob.crh),
            },
            &mut delay,
            &mut afio.mapr,
            &mut rcc.apb2,
            clocks,
        );


        // Set up the timer
        let timer = Timer::tim2(dp.TIM2, &clocks, &mut rcc.apb1)
            .start_count_down(2.hz());


        // Set up button scanner
        let scanner = Scanner::new(
                [
                    gpioa.pa4.into_pull_up_input(&mut gpioa.crl).downgrade(),
                    gpioa.pa3.into_pull_up_input(&mut gpioa.crl).downgrade(),
                    gpioa.pa2.into_pull_up_input(&mut gpioa.crl).downgrade(),
                    gpioa.pa1.into_pull_up_input(&mut gpioa.crl).downgrade(),
                    gpioa.pa0.into_pull_up_input(&mut gpioa.crl).downgrade(),
                    gpioc.pc15.into_pull_up_input(&mut gpioc.crh).downgrade(),
                    gpioc.pc14.into_pull_up_input(&mut gpioc.crh).downgrade(),
                ],
                [
                    gpiob.pb9.into_push_pull_output(&mut gpiob.crh).downgrade(),
                    gpiob.pb8.into_push_pull_output(&mut gpiob.crh).downgrade(),
                    gpiob.pb5.into_push_pull_output(&mut gpiob.crl).downgrade(),
                    gpiob.pb6.into_push_pull_output(&mut gpiob.crl).downgrade(),
                    gpiob.pb7.into_push_pull_output(&mut gpiob.crl).downgrade(),
                ]
            );

        let mut syst = delay.free();

        syst.set_reload(0xffff_ffff);
        syst.clear_current();
        syst.enable_counter();

        let model = Model::new(&layout::AIRCRAFT_SELECTION_LAYER);

        init::LateResources {
            display,
            usb_bus: USB_BUS.as_ref().unwrap(),
            usb_serial,
            usb_joystick,
            usb_device,
            timer,
            scanner,
            model,
            syst,
        }
    }

    #[idle(resources = [scanner, usb_serial, usb_joystick, timer, display, model, syst])]
    fn idle(ctx: idle::Context) -> ! {
        let mut r = ctx.resources;
        let mut layer_stack = ArrayVec::<[&'static layout::Layer; 30]>::new();
        loop {
            let current_layer = r.model.current_layer;

            let newly_pressed = r.scanner.scan(pac::SYST::get_current());

            let report = build_button_report(r.scanner, r.model.current_layer);

            let mut layer_changed = false;
            let mut layer_changed = false;
            for (y, x) in newly_pressed {
                layer_changed |= handle_action(
                    r.model,
                    &current_layer[y as usize][x as usize].action
                );
            }

            if layer_changed {
                r.scanner.notify_layer_change();
            }
            r.display.maybe_redraw(r.model);

            r.usb_joystick.lock(|joy| {
                joy.device_mut().set_report(report.clone());
            });

            r.usb_joystick.lock(|joy| {
                joy.write(&report.as_bytes())
            });

            if let Ok(()) = r.timer.wait() {
                let pressed = r.scanner.pressed().clone();
                r.usb_serial.lock(|serial| {
                    uwriteln!(
                        DirtyWriter(serial),
                        "Pressed now:\r",
                    ).unwrap();
                    for x in 0..GRID_SIZE_X {
                        for y in 0..GRID_SIZE_Y {
                            if pressed[y][x] {
                                uwriteln!(
                                    DirtyWriter(serial),
                                    "({}, {})\r",
                                    x, y
                                ).unwrap();
                            }
                        }
                    }
                });
            }
        }
    }

    #[task(binds = USB_LP_CAN_RX0, resources = [usb_device, usb_joystick, usb_serial])]
    fn usb_lp_can_rx0(cx: usb_lp_can_rx0::Context) {
        usb_poll(cx.resources.usb_device, cx.resources.usb_serial, cx.resources.usb_joystick);
    }

    #[task(binds = USB_HP_CAN_TX, resources = [usb_device, usb_joystick, usb_serial])]
    fn usb_hp_can_tx0(cx: usb_hp_can_tx0::Context) {
        usb_poll(cx.resources.usb_device, cx.resources.usb_serial, cx.resources.usb_joystick);
    }
};


fn handle_action(model: &mut Model, action: &Action) -> bool{
    match action {
        Action::ChangeLayer(new) => {
            model.push_layer(new);
            true
        },
        Action::Back => {
            model.pop_layer();
            true
        }
        Action::EnterCalculator => {
            model.show_calculator();
            true
        }
        Action::CloseCalculator => {
            model.hide_calculator();
            true
        }
        Action::CalcAction(CalcAction::Erase) => {
            model.calculator.erase();
            false
        }
        Action::CalcAction(CalcAction::PushDigit(digit)) => {
            model.calculator.push_char(*digit);
            false
        }
        Action::Nothing => {false}
    }
}

fn usb_poll<B: usb_device::bus::UsbBus>(
    usb_dev: &mut UsbDevice<'static, B>,
    serial: &mut SerialPort<'static, B>,
    joystick: &mut hid::HidClass<'static, B, joystick::Joystick>,
) {
    if !usb_dev.poll(&mut [serial, joystick]) {
        return;
    }
    joystick.poll();
    let mut buf = [0;10];
    match serial.read(&mut buf) {
        Ok(_) => {},
        Err(UsbError::WouldBlock) => {},
        e => panic!("USB read error: {:?}", e)
    }
}

pub struct DirtyWriter<'a, B: 'static + usb_device::bus::UsbBus>(
    &'a mut SerialPort<'static, B>
);

impl<'a, B: usb_device::bus::UsbBus> ufmt::uWrite for DirtyWriter<'a, B> {
    type Error = usb_device::UsbError;
    fn write_str(&mut self, s: &str) -> Result<(), Self::Error> {
        match self.0.write(&s.as_bytes()) {
            Ok(_) => Ok(()),
            Err(UsbError::WouldBlock) => Ok(()),
            Err(e) => Err(e)
        }
    }
}


pub fn build_button_report(
    scanner: &Scanner<Pxx<Input<PullUp>>, Pxx<Output<PushPull>>>,
    layer: &layout::Layer,
) -> joystick::ButtonReport {
    let mut report = joystick::ButtonReport::default();
    for x in 0..GRID_SIZE_X {
        for y in 0..GRID_SIZE_Y {
            if scanner.pressed()[y as usize][x as usize] {
                match layer[y as usize][x as usize].button {
                    Some(button) => {
                        report.buttons[button as usize] = true
                    }
                    None => {}
                }
            }
        }
    }
    report
}
