use embedded_hal::digital::v2::OutputPin;
use stm32f1xx_hal::{
    prelude::*,
    pac,
    gpio::{
        gpioa,
        gpiob,
        Alternate,
        PushPull,
        Floating,
        Output,
        Input
    },
    afio::MAPR,
    spi::{self, Spi, Mode, Polarity, Phase},
    rcc::{APB2, Clocks},
    delay::Delay,
};
use embedded_graphics::fonts::{Text, Font6x8};
use embedded_graphics::prelude::*;
use embedded_graphics::primitives::rectangle::Rectangle;
use embedded_graphics::pixelcolor::Rgb565;
use embedded_graphics::style::{TextStyle, PrimitiveStyleBuilder};
use st7735_lcd;
use st7735_lcd::Orientation;
use arrayvec::ArrayVec;

use crate::layout;
use crate::model::Model;
use crate::calc::Calculator;

const CHAR_W: i32 = 6;
const CHAR_H: i32 = 8;

pub type LcdPins = (
    gpioa::PA5<Alternate<PushPull>>,
    gpioa::PA6<Input<Floating>>,
    gpioa::PA7<Alternate<PushPull>>
);
pub type Lcd = st7735_lcd::ST7735<
    Spi<pac::SPI1, spi::Spi1NoRemap, LcdPins>,
    gpiob::PB1<Output<PushPull>>,
    gpiob::PB0<Output<PushPull>>,
>;


pub struct DisplayPins {
    pub sck: gpioa::PA5<Alternate<PushPull>>,
    pub miso: gpioa::PA6<Input<Floating>>,
    pub mosi: gpioa::PA7<Alternate<PushPull>>,

    pub rst: gpiob::PB0<Output<PushPull>>,
    pub dc: gpiob::PB1<Output<PushPull>>,

    pub backlight: gpiob::PB10<Output<PushPull>>,
}

pub struct Display {
    device: Lcd,
    prev_areas: ArrayVec<[(Point, Point); 32]>,
    backlight: gpiob::PB10<Output<PushPull>>,
    last_layer: Option<&'static layout::Layer>,
    last_calculator: Option<Calculator>,
}

impl Display {
    pub fn new(
        spi_dev: pac::SPI1,
        mut pins: DisplayPins,
        delay: &mut Delay,
        mapr: &mut MAPR,
        apb2: &mut APB2,
        clocks: Clocks,
    ) -> Self {
        pins.backlight.set_high().unwrap();

        let spi = Spi::spi1(
            spi_dev,
            (pins.sck, pins.miso, pins.mosi),
            mapr,
            Mode {
                polarity: Polarity::IdleLow,
                phase: Phase::CaptureOnFirstTransition,
            },
            16.mhz(),
            clocks,
            apb2,
        );

        let mut device = st7735_lcd::ST7735::new(
            spi,
            pins.dc,
            pins.rst,
            true,
            false,
            160,
            128
        );

        device.init(delay).unwrap();
        device.set_orientation(&Orientation::Portrait).unwrap();
        let style = PrimitiveStyleBuilder::new().fill_color(Rgb565::BLACK).build();
        let black_backdrop = Rectangle::new(Point::new(0, 0), Point::new(128, 160))
            .into_styled(style);
        black_backdrop.draw(&mut device).unwrap();

        Self {
            device,
            backlight: pins.backlight,
            prev_areas: ArrayVec::new(),
            last_layer: None,
            last_calculator: None,
        }
    }

    pub fn maybe_redraw(&mut self, model: &Model) {
        let is_new_layer = self.last_layer
            .map(|o| !core::ptr::eq(o, model.current_layer))
            .unwrap_or(true);

        if is_new_layer {
            let layer = model.current_layer;
            self.last_layer = Some(layer);
            // Clear the previous drawing
            let style = PrimitiveStyleBuilder::new()
                .fill_color(Rgb565::BLACK)
                .build();
            for area in self.prev_areas.iter() {
                let black_backdrop = Rectangle::new(area.0, area.1)
                    .into_styled(style);
                black_backdrop.draw(&mut self.device).unwrap();
            }
            self.prev_areas.clear();

            // Set the location of labels corresponding to each button
            enum Direction {
                Left(i32),
                Right(i32),
            }

            let label_y_coords = [0,  8, 36, 64, 93, 121, 149];
            let label_x_coords = [20, 0, 0,  0,  0,  0,   0  ];

            let label_locations = [
                ((Direction::Left(label_x_coords[0]),  label_y_coords[0]), (0, 0)),
                ((Direction::Left(label_x_coords[1]),  label_y_coords[1]), (0, 1)),
                ((Direction::Left(label_x_coords[2]),  label_y_coords[2]), (0, 2)),
                ((Direction::Left(label_x_coords[3]),  label_y_coords[3]), (0, 3)),
                ((Direction::Left(label_x_coords[4]),  label_y_coords[4]), (0, 4)),
                ((Direction::Left(label_x_coords[5]),  label_y_coords[5]), (0, 5)),
                ((Direction::Left(label_x_coords[6]),  label_y_coords[6]), (0, 6)),

                ((Direction::Right(128-label_x_coords[0]),  label_y_coords[0]), (1, 0)),
                ((Direction::Right(128-label_x_coords[1]),  label_y_coords[1]), (1, 1)),
                ((Direction::Right(128-label_x_coords[2]),  label_y_coords[2]), (1, 2)),
                ((Direction::Right(128-label_x_coords[3]),  label_y_coords[3]), (1, 3)),
                ((Direction::Right(128-label_x_coords[4]),  label_y_coords[4]), (1, 4)),
                ((Direction::Right(128-label_x_coords[5]),  label_y_coords[5]), (1, 5)),
                ((Direction::Right(128-label_x_coords[6]),  label_y_coords[6]), (1, 6)),
            ];

            // Draw the button labels
            for (pos, label_idx) in label_locations.iter() {
                let label_text = layer[label_idx.0][label_idx.1].label;

                let text_width = label_text.chars().count() as i32 * 6;
                let x = match pos.0 {
                    Direction::Left(x) => x,
                    Direction::Right(x) => x - text_width
                };
                let y = pos.1;

                let true_pos = Point::new(x, y);

                let t = Text::new(label_text, true_pos)
                    .into_styled(TextStyle::new(Font6x8, Rgb565::WHITE));
                t.draw(&mut self.device).unwrap();

                self.prev_areas.push((true_pos, true_pos + Point::new(text_width, 8)));
            }
        }

        if model.calculator_active {
            self.maybe_redraw_calculator(&model.calculator);
        }
        else {
            self.last_calculator = None;
        }
    }

    pub fn set_backlight(&mut self, state: bool) {
        if state {
            self.backlight.set_high().unwrap()
        }
        else {
            self.backlight.set_high().unwrap()
        }
    }

    fn maybe_redraw_calculator(&mut self, calc: &Calculator) {
        if Some(calc) != self.last_calculator.as_ref() {
            self.last_calculator = Some(calc.clone());

            let start = Point::new(CHAR_W*6, CHAR_H);

            let t = Text::new(&calc.operands, start)
                .into_styled(TextStyle::new(Font6x8, Rgb565::WHITE));
            t.draw(&mut self.device).unwrap();
        }
    }
}
