use crate::calc;

#[derive(Clone, PartialEq)]
pub enum CalcAction {
    PushDigit(char),
    Erase,
}

#[derive(Clone, PartialEq)]
pub enum Action {
    Nothing,
    Back,
    ChangeLayer(&'static Layer),
    EnterCalculator,
    CalcAction(CalcAction),
    CloseCalculator,
}

use Action::*;

#[derive(Clone, PartialEq)]
pub struct Button {
    pub action: Action,
    pub button: Option<u8>,
    pub label: &'static str
}

const fn b(action: Action, button: Option<u8>, label: &'static str) -> Button {
    Button {
        action, button, label
    }
}

macro_rules! key {
    ($key:expr) => {Some($key as u8)}
}

pub type Layer = [[Button;7]; 5];

pub const AIRCRAFT_SELECTION_LAYER: &'static Layer =
&[
    [
        b(ChangeLayer(&viggen::MAIN),        None, "Viggen"),
        b(ChangeLayer(&harrier::MAIN_LAYER), None, "Harrier"),
        b(ChangeLayer(&f16::MAIN),           None, "F16"),
        b(Nothing,                           None, "KA-50"),
        b(EnterCalculator,                   None, "Calculator"),
        b(Nothing,                           None, "-"),
        b(Nothing,                           None, "-"),
    ],
    [
        b(Nothing,                           None, "-"),
        b(Nothing,                           None, "-"),
        b(Nothing,                           None, "-"),
        b(Nothing,                           None, "-"),
        b(Nothing,                           None, "-"),
        b(Nothing,                           None, "-"),
        b(Nothing,                           None, "-"),
    ],
    [
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
    ],
    [
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
    ],
    [
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
        b(Nothing,                           None, ""),
    ],
];

mod viggen {
    use super::*;
    #[repr(u8)]
    pub enum Key {
        MasterModeBER,
        MasterModeNAV,
        MasterModeANF,
        MasterModeSPA,
        MasterModeLandNAV,
        MasterModeLandPO,

        HudHeight,

        Afk,
        ThrustReverser,

        B1,
        B2,
        B3,
        B4,
        B5,
        B6,
        B7,
        B8,
        B9,
        Bx,
        LS,
        LM,

        Keypad0,
        Keypad1,
        Keypad2,
        Keypad3,
        Keypad4,
        Keypad5,
        Keypad6,
        Keypad7,
        Keypad8,
        Keypad9,

        AttitudeHold,
        AltitutdeHold,
    }

    macro_rules! keypad_row1 {
        () => {
            [
                b(Nothing, Some(Keypad1 as u8), ""),
                b(Nothing, Some(Keypad4 as u8), ""),
                b(Nothing, Some(Keypad7 as u8), ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
            ]
        }
    }
    macro_rules! keypad_row2 {
        () => {
            [
                b(Nothing, Some(Keypad2 as u8), ""),
                b(Nothing, Some(Keypad5 as u8), ""),
                b(Nothing, Some(Keypad8 as u8), ""),
                b(Nothing, Some(Keypad0 as u8), ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
            ]
        }
    }
    macro_rules! keypad_row3 {
        () => {
            [
                b(Nothing, Some(Keypad3 as u8), ""),
                b(Nothing, Some(Keypad6 as u8), ""),
                b(Nothing, Some(Keypad9 as u8), ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
            ]
        }
    }

    use Key::*;

    pub const MAIN: &'static Layer =
    &[
        [
            b(Nothing,                  None,                      "-"),
            b(ChangeLayer(MASTER_MODE), None,                      "Master Mode"),
            b(Nothing,                  None,                      "-"),
            b(Nothing,                  None,                      "-"),
            b(Nothing,                  None,                      "-"),
            b(Nothing,                  Some(AttitudeHold as u8),  "Att Hold"),
            b(Nothing,                  Some(AltitutdeHold as u8), "Alt Hold"),
        ],
        [
            b(Back,                     None,                      "Back"),
            b(ChangeLayer(LANDING),     None,                      "Landing"),
            b(ChangeLayer(WAYPOINT),    None,                      "NAV"),
            b(Nothing,                  None,                      "-"),
            b(Nothing,                  None,                      "-"),
            b(Nothing,                  None,                      "-"),
            b(Nothing,                  Some(HudHeight as u8),     "HUD H"),
        ],
        keypad_row1![],
        keypad_row2![],
        keypad_row3![]
    ];

    pub const LANDING: &'static Layer =
        &[
            [
                b(Nothing, None,                          "-"),
                b(Nothing, None,                          "-"),
                b(Nothing, None,                          "-"),
                b(Nothing, None,                          "-"),
                b(Nothing, None,                          "-"),
                b(Nothing, Some(MasterModeLandNAV as u8), "LAND NAV"),
                b(Nothing, Some(MasterModeLandPO as u8),  "Land P/O"),
            ],
            [
                b(Back,    None,                          "Back"),
                b(Nothing, Some(Afk as u8),               "AFK"),
                b(Nothing, Some(ThrustReverser as u8),    "Rev"),
                b(Nothing, None,                          "-"),
                b(Nothing, None,                          "-"),
                b(Nothing, None,                          "-"),
                b(Nothing, Some(HudHeight as u8),         "HUD H"),
            ],
            keypad_row1![],
            keypad_row2![],
            keypad_row3![]
        ];

    pub const WAYPOINT: &'static Layer =
        &[
            [
                b(Nothing,    Some(LS as u8), "LS"),
                b(Nothing,    Some(B1 as u8), "B1"),
                b(Nothing,    Some(B2 as u8), "B2"),
                b(Nothing,    Some(B3 as u8), "B3"),
                b(Nothing,    Some(B4 as u8), "B4"),
                b(Nothing,    Some(B5 as u8), "B5"),
                b(Nothing,    Some(B6 as u8), "B6"),
            ],
            [
                b(Back,    None,           "Back"),
                b(Nothing,    Some(B7 as u8), "B7"),
                b(Nothing,    Some(B8 as u8), "B8"),
                b(Nothing,    Some(B9 as u8), "B9"),
                b(Nothing,    Some(Bx as u8), "Bx"),
                b(Nothing,    Some(LM as u8), "LMål"),
                b(Nothing, None,           "-"),
            ],
            keypad_row1![],
            keypad_row2![],
            keypad_row3![]
        ];

    pub const MASTER_MODE: &'static Layer =
    &[
        [
            b(Nothing,  None,                          "-"),
            b(Back,     Some(MasterModeBER as u8),     "BER"),
            b(Back,     Some(MasterModeNAV as u8),     "NAV"),
            b(Back,     Some(MasterModeANF as u8),     "ANF"),
            b(Back,     Some(MasterModeSPA as u8),     "SPA"),
            b(Back,     Some(MasterModeLandNAV as u8), "LAND NAV"),
            b(Back,     Some(MasterModeLandPO as u8),  "Land P/O"),
        ],
        [
            b(Back,     None,                          "Back"),
            b(Nothing,  None,                          "-"),
            b(Nothing,  None,                          "-"),
            b(Nothing,  None,                          "-"),
            b(Nothing,  None,                          "-"),
            b(Nothing,  None,                          "-"),
            b(Nothing,  None,                          "-"),
        ],
        keypad_row1![],
        keypad_row2![],
        keypad_row3![]
    ];
}

mod harrier {
    use super::*;
    #[repr(u8)]
    pub enum Key {
        AfcToggle,
        AltHold,

        Keypad0,
        Keypad1,
        Keypad2,
        Keypad3,
        Keypad4,
        Keypad5,
        Keypad6,
        Keypad7,
        Keypad8,
        Keypad9,
        KeypadRcl,
        KeypadEnter,
    }

    use Key::*;

    macro_rules! keypad_row1 {
        () => {
            [
                b(Nothing, Some(Keypad1 as u8), ""),
                b(Nothing, Some(Keypad4 as u8), ""),
                b(Nothing, Some(Keypad7 as u8), ""),
                b(Nothing, Some(KeypadRcl as u8), ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
            ]
        }
    }
    macro_rules! keypad_row2 {
        () => {
            [
                b(Nothing, Some(Keypad8 as u8), ""),
                b(Nothing, Some(Keypad5 as u8), ""),
                b(Nothing, Some(Keypad2 as u8), ""),
                b(Nothing, Some(Keypad0 as u8), ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
            ]
        }
    }
    macro_rules! keypad_row3 {
        () => {
            [
                b(Nothing, Some(Keypad9 as u8), ""),
                b(Nothing, Some(Keypad6 as u8), ""),
                b(Nothing, Some(Keypad3 as u8), ""),
                b(Nothing, Some(KeypadEnter as u8), ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
            ]
        }
    }

    pub const MAIN_LAYER: &'static Layer =
    &[
        [
            b(Nothing, None,                  "-"),
            b(Nothing, Some(AfcToggle as u8), "AFC"),
            b(Nothing, Some(AltHold as u8),   "Alt Hold"),
            b(Nothing, None,                  "-"),
            b(Nothing, None,                  "-"),
            b(Nothing, None,                  "-"),
            b(Nothing, None,                  "-"),
        ],
        [
            b(Back,    None,                  "Back"),
            b(Nothing, None,                  "-"),
            b(Nothing, None,                  "-"),
            b(Nothing, None,                  "-"),
            b(Nothing, None,                  "-"),
            b(Nothing, None,                  "-"),
            b(Nothing, None,                  "-"),
        ],
        keypad_row1![],
        keypad_row2![],
        keypad_row3![],
    ];
}

mod f16 {
    use super::*;
    #[repr(u8)]
    pub enum Key {
        TwsRws,
        ScanAzimuth,
        RangeUp,
        RangeDown,
        Jettison,

        Keypad0,
        Keypad1,
        Keypad2,
        Keypad3,
        Keypad4,
        Keypad5,
        Keypad6,
        Keypad7,
        Keypad8,
        Keypad9,
        KeypadRcl,
        KeypadEnter,

        MasterModeAA,
        MasterModeAG,

        HSDRangeUp,
        HSDRangeDown,
        HSDCenter,

        FCRZoom,
    }

    use Key::*;
    macro_rules! keypad_row1 {
        () => {
            [
                b(Nothing, Some(Keypad1 as u8), ""),
                b(Nothing, Some(Keypad4 as u8), ""),
                b(Nothing, Some(Keypad7 as u8), ""),
                b(Nothing, Some(KeypadRcl as u8), ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
            ]
        }
    }
    macro_rules! keypad_row2 {
        () => {
            [
                b(Nothing, Some(Keypad2 as u8), ""),
                b(Nothing, Some(Keypad5 as u8), ""),
                b(Nothing, Some(Keypad8 as u8), ""),
                b(Nothing, Some(Keypad0 as u8), ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
            ]
        }
    }
    macro_rules! keypad_row3 {
        () => {
            [
                b(Nothing, Some(Keypad3 as u8), ""),
                b(Nothing, Some(Keypad6 as u8), ""),
                b(Nothing, Some(Keypad9 as u8), ""),
                b(Nothing, Some(KeypadEnter as u8), ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
                b(Nothing, None, ""),
            ]
        }
    }

    pub const MAIN: &'static Layer =
    &[
        [
            b(Nothing,          None,               "-"),
            b(ChangeLayer(A2A), None,               "A2A"),
            b(Nothing,          None,               "-"),
            b(Nothing,          None,               "-"),
            b(Nothing,          None,               "-"),
            b(Nothing,          None,               "-"),
            b(Nothing,          None,               "-"),
        ],
        [
            b(Back,             None,               "Back"),
            b(Nothing,          key!(MasterModeAA), "AA"),
            b(Nothing,          key!(MasterModeAG), "AG"),
            b(Nothing,          None,               "-"),
            b(Nothing,          None,               "-"),
            b(Nothing,          None,               "-"),
            b(Nothing,          None,               "-"),
        ],
        keypad_row1![],
        keypad_row2![],
        keypad_row3![],
    ];

    pub const A2A: &'static Layer =
    &[
        [
            b(Nothing,  None,               "-"),
            b(Nothing,  key!(TwsRws),       "RWS/TWS"),
            b(Nothing,  key!(RangeUp),      "R+"),
            b(Nothing,  key!(RangeDown),    "R-"),
            b(Nothing,  key!(ScanAzimuth),  "Ax"),
            b(Nothing,  key!(FCRZoom),      "Zoom"),
            b(Nothing,  None,               "-"),
        ],
        [
            b(Back,     None,               "Back"),
            b(Nothing,  key!(HSDRangeUp),   "HR+"),
            b(Nothing,  key!(HSDRangeDown), "HR-"),
            b(Nothing,  key!(HSDCenter),    "HSD Cent"),
            b(Nothing,  None,               "-"),
            b(Nothing,  key!(MasterModeAA), "Mode"),
            b(Nothing,  key!(Jettison),     "JETTISON"),
        ],
        keypad_row1![],
        keypad_row2![],
        keypad_row3![],
    ];
}



pub mod calculator {
    use super::*;

    use super::CalcAction::*;

    pub const MAIN_LAYER: &'static Layer =
    &[
        [
            b(CloseCalculator, None, "Close"),
            b(Nothing,         None, "add"),
            b(Nothing,         None, "sub"),
            b(Nothing,         None, "qfe"),
            b(Nothing,         None, "-"),
            b(Nothing,         None, "-"),
            b(Nothing,         None, "-"),
        ],
        [
            b(Nothing,         None, "-"),
            b(Nothing,         None, "-"),
            b(Nothing,         None, "-"),
            b(Nothing,         None, "-"),
            b(Nothing,         None, "-"),
            b(Nothing,         None, "-"),
            b(Nothing,         None, "-"),
        ],
        [
            b(CalcAction(PushDigit('1')), None, ""),
            b(CalcAction(PushDigit('4')), None, ""),
            b(CalcAction(PushDigit('7')), None, ""),
            b(CalcAction(Erase), None, ""),
            b(Nothing, None, ""),
            b(Nothing, None, ""),
            b(Nothing, None, ""),
        ],
        [
            b(CalcAction(PushDigit('2')), None, ""),
            b(CalcAction(PushDigit('5')), None, ""),
            b(CalcAction(PushDigit('8')), None, ""),
            b(CalcAction(PushDigit('0')), None, ""),
            b(Nothing, None, ""),
            b(Nothing, None, ""),
            b(Nothing, None, ""),
        ],
        [
            b(CalcAction(PushDigit('3')), None, ""),
            b(CalcAction(PushDigit('6')), None, ""),
            b(CalcAction(PushDigit('9')), None, ""),
            b(Nothing, None, ""),
            b(Nothing, None, ""),
            b(Nothing, None, ""),
            b(Nothing, None, ""),
        ]
    ];
}

